use warnings;
use strict;
use Math::BigInt;

open my $erl_in, '<&=3' or die "failed opening Erlang input pipe FD 3: $?";
open my $erl_out, '>&=4' or die "failed opening Erlang output pipe FD 4: $?";
#open STDOUT, ">>/tmp/perlang5.log" or die "failed opening output: $?";
select($erl_out);
$| = 1;
select(STDOUT);
$| = 1;
my %options = ('debug' => 1);

sub debug($) {
    print(@_) if $options{'debug'};
}

sub erl_read_decode($);
sub erl_read_decode($) {
    my $tag = shift or die;
    debug("erl_read_decode($tag)\n");
    my $buf;
    if ($tag == 97) {
        read($erl_in, $buf, 1) or die "Unable to read 1 byte integer: $?. Stopped";
        my $int = unpack('C', $buf);
        return $int;
    } elsif ($tag == 100) {
        read($erl_in, $buf, 2) or die "Unable to read 2 bytes of atom-length: $?. Stopped";
        my $len = unpack('n', $buf);
        read($erl_in, $buf, $len) or die "Unable to read $len bytes of atom: $?. Stopped";
        my $atom = unpack('A*', $buf);
        return $atom;
    } elsif ($tag == 109) {
        read($erl_in, $buf, 4) or die "Unable to read 4 bytes of binary-length: $?. Stopped";
        my $len = unpack('N', $buf);
        read($erl_in, $buf, $len) or die "Unable to read $len bytes of binary: $?. Stopped";
        my $str = unpack('A*', $buf);
        return $str;
    } elsif ($tag == 116) {
        read($erl_in, $buf, 4) or die "Unable to read 4 bytes of map-size: $?. Stopped";
        my $len = unpack('N', $buf);
        my $map = {};
        foreach my $i (1..$len) {
            read($erl_in, $buf, 1) or die "Unable to read ${i}th key-tag of $len in map: $?. Stopped";
            my $key_tag = unpack('C', $buf);
            my $key = erl_read_decode($key_tag);
            my $val_tag = unpack('C', $buf);
            my $val = erl_read_decode($val_tag);
            $map->{$key} = $val;
        }
        debug "Read past end of map\n";
        while (my $x = read($erl_in, $buf, 1)) {
            debug "Got: $x\n";
            debug "Read past end of map\n";
        }
        return $map;
    } else {
        die "Unhandled tag $tag when decoding from Erlang. Stopped";
    }
}

sub erl_atom($$$) {
    my ($pat_ref, $list_ref, $val) = @_;
    ${$pat_ref} .= 'C C/A* ';
    push(@{$list_ref}, (119, $val));
}

sub erl_binstring($$$) {
    my ($pat_ref, $list_ref, $val) = @_;
    ${$pat_ref} .= 'C N/A* ';
    push(@{$list_ref}, (109, $val));
}

sub erl_integer($$$) {
    my ($pat_ref, $list_ref, $val) = @_;
    ${$pat_ref} .= 'C N ';
    push(@{$list_ref}, (98, $val));
}

sub erl_float($$$) {
    my ($pat_ref, $list_ref, $val) = @_;
    ${$pat_ref} .= 'C d> ';
    push(@{$list_ref}, (70, $val));
}

sub erl_tuple($$$) {
    my ($pat_ref, $list_ref, $val) = @_;
    ${$pat_ref} .= 'C C ';
    push(@{$list_ref}, (104, $val));
}

sub erl_encode_scalar($);
sub erl_encode_scalar($) {
    $_ = shift or die "No arguments passed to erl_encode_scalar(@_). Stopped";
    my $looks_non_numeric;
    {
        no warnings 'numeric';
        $looks_non_numeric = ($_ == 0 and $_ ne '0');
    }
    my $pack_pattern = '';
    my @pack_list = ();

    print "ref=".ref($_)."\n";
    if (ref($_) eq 'Regexp') {
        # Regular expression
        erl_tuple(\$pack_pattern, \@pack_list, 2);
        erl_atom(\$pack_pattern, \@pack_list, 'regexp');
        erl_binstring(\$pack_pattern, \@pack_list, $_);
    } elsif (ref($_) eq 'SCALAR') {
        erl_tuple(\$pack_pattern, \@pack_list, 2);
        erl_atom(\$pack_pattern, \@pack_list, 'reference');
        $pack_pattern .= 'a* ';
        push(@pack_list, erl_encode_scalar(${$_}));
    } elsif (Math::BigInt->is_inf($_)) {
        # special-case for IEEE infinity
        # Detecting NaN seems dicier, so we'll just leave it as a string
        erl_atom(\$pack_pattern, \@pack_list, 'infinity');
    } elsif ($looks_non_numeric) {
        # string
        print "looks non-numeric: $_\n";
        erl_tuple(\$pack_pattern, \@pack_list, 2);
        erl_atom(\$pack_pattern, \@pack_list, 'string');
        erl_binstring(\$pack_pattern, \@pack_list, $_);
    } elsif (/^-?\d+$/) {
        # integer
        print "looks like an integer: $_\n";
        erl_tuple(\$pack_pattern, \@pack_list, 2);
        erl_atom(\$pack_pattern, \@pack_list, 'number');
        erl_integer(\$pack_pattern, \@pack_list, $_);
    } else {
        # float
        print "looks like a float: $_\n";
        erl_tuple(\$pack_pattern, \@pack_list, 2);
        erl_atom(\$pack_pattern, \@pack_list, 'number');
        erl_float(\$pack_pattern, \@pack_list, $_);
    }
    return pack($pack_pattern, @pack_list);
}

sub read_version_and_tag() {
    my $erl_buf;
    read($erl_in, $erl_buf, 2) or return undef;
    my ($version, $tag) = unpack('C C', $erl_buf);
    die "Bad version $version when decoding from Erlang. Stopped" unless ($version == 131);
    return $tag;
}

sub read_options() {
    my $tag = read_version_and_tag();
    die "Expecting an options map but got $tag. Stopped" unless ($tag == 116);
    my $options = erl_read_decode($tag);
    %options = %$options;
    debug "Perl got options ".join(',', %options)."\n";
    my $reply = pack('C C C/A*', (131, 119, 'ok'));
    print $erl_out $reply;
    debug "Perl sent reply\n";
}


#read_options();
debug "Perl waiting for Erlang input\n";
while (my $tag = read_version_and_tag()) {
    my $data = erl_read_decode($tag);
    print "Perl got input '$data'\n";
    my $result = eval $data;
    my $reply;
    if (defined($result)) {
        print "Perl produced output '$result'\n";
        $reply = pack('C a*', (131, erl_encode_scalar($result)));
    } else {
        print "Perl produced undef\n";
        $reply = pack('C C C/A*', (131, 119, 'undefined'));
    }
    print $erl_out $reply;
}
