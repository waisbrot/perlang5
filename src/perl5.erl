-module(perl5).

%% API exports
-export([start_link/0, stop/1, eval/2]).

-define(LIBRARY, "perlang5").

%%====================================================================
%% API functions
%%====================================================================
start_link() ->
    perl5_port:start_link().

stop(Pid) ->
    perl5_port:stop(Pid).

eval(Pid, String) ->
    perl5_port:eval(Pid, String).

%%====================================================================
%% Internal functions
%%====================================================================


%%====================================================================
%% Tests
%%====================================================================
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

basic_start_stop_test() ->
    {ok, Perl} = start_link(),
    ?assert(is_process_alive(Perl)),
    stop(Perl),
    ?assertNot(is_process_alive(Perl)).

basic_call_test() ->
    {ok, Perl} = start_link(),
    ?assertEqual({ok, {number, 1}}, eval(Perl, <<"1">>)),
    stop(Perl).

setup() ->
    {ok, Perl} = start_link(),
    Perl.

cleanup(Perl) ->
    stop(Perl).

scalars_test_generator(Perl) ->
    lists:map(fun ({Code, Expect}) -> ?_assertEqual({ok, Expect}, eval(Perl, Code)) end,
              [
               {<<"14">>, {number, 14}}
              ,{<<"\"15\"">>, {number, 15}}
              ,{<<"'\"16\"'">>, {string, <<"\"16\"">>}}
              ,{<<"1.4">>, {number, 1.4}}
              ,{<<"'1.5'">>, {number, 1.5}}
              ,{<<"'\"1.6'">>, {string, <<"\"1.6">>}}
              ,{<<"\"Hello world\"">>, {string, <<"Hello world">>}}
              ,{<<"'Hello world!'">>, {string, <<"Hello world!">>}}
              ,{<<"\"\\\"Hello world?\\\"\"">>, {string, <<"\"Hello world?\"">>}}
              ,{<<"undef">>, undefined}
              ,{<<"9**9**9">>, infinity}
              ,{<<"-9**9**9">>, infinity}
              ,{<<"my $a = 1; $a">>, {number, 1}}
              ,{<<"my $b = 2048; \"$b\"">>, {number, 2048}}
              ,{<<"my $c = qr/hel*o/; $c">>, {regexp, <<"(?^:hel*o)">>}}
              ,{<<"my $a = 1; \\$a">>, {reference, {number, 1}}}
              ]).

read_scalars_test_() ->
    {setup, fun setup/0, fun cleanup/1, fun scalars_test_generator/1}.

-endif.
