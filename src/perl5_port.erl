-module(perl5_port).
-behaviour(gen_statem).
-export([start_link/0, init/1, callback_mode/0]).
-export([initializing/3, waiting_for_init/3, ready/3, waiting/3]).
-export([stop/1, eval/2]).

-record(data, {
               perl
              ,caller
              }).

-define(DEBUG(Fmt, Args), io:format(user, Fmt, Args)).

%%====================================================================
%% API
%%====================================================================

start_link() ->
    gen_statem:start_link(?MODULE, [], []).

stop(Perl) ->
    gen_statem:stop(Perl).

eval(Perl, Code) ->
    gen_statem:call(Perl, {eval, Code}).

%%====================================================================
%% Gen StateM callbacks
%%====================================================================

init([]) ->
    {ok, initializing, #data{}, [{next_event, internal, initialize}]}.

callback_mode() ->
    state_functions.

initializing(internal, initialize, Data) ->
    PerlCode = find_perl_code(),
    PerlBin = find_perl_bin(),
    Perl = open_port({spawn_executable, PerlBin}, [
                                                   {args, ["--", PerlCode]}
                                                  ,nouse_stdio
                                                  ,binary
                                                  ,exit_status
                                                  ]),
%    Options = #{debug => true},
%    Encoded = encode(Options),
%    ?DEBUG("Sending ~p as ~p~n", [Options, Encoded]),
%    Perl ! {self(), {command, Encoded}},
%    ?DEBUG("Erlang waiting for Perl init reply~n", []),
%    {next_state, waiting_for_init, Data#data{perl=Perl}};
    {next_state, ready, Data#data{perl=Perl}};
initializing(_Type, _Message, _Data) ->
    ?DEBUG("initializing: Postpone unexpected message ~p~n", [_Message]),
    {keep_state_and_data, postpone}.

waiting_for_init(info, {Perl, {data, Reply}}, Data = #data{perl=Perl}) ->
    ?DEBUG("got reply ~p~n", [Reply]),
    case decode(Reply) of
        ok ->
            {next_state, ready, Data};
        Other ->
            {stop, {bad_reply, Other}}
    end;
waiting_for_init(_Type, _Message, _Data) ->
    ?DEBUG("waiting_for_init: Postpone unexpected message ~p~n", [_Message]),
    {keep_state_and_data, postpone}.

ready({call, From}, {eval, Code}, Data) ->
    Encoded = encode(Code),
    io:format(user, "DEBUG Sending ~p as ~p~n", [Code, Encoded]),
    Data#data.perl ! {self(), {command, Encoded}},
    {next_state, waiting, Data#data{caller=From}};
ready(info, {Perl, {exit_status, 0}}, #data{perl=Perl}) ->
    {stop, {shutdown, perl_exit}};
ready(info, {Perl, {exit_status, Status}}, #data{perl=Perl}) ->
    {stop, {perl_died, Status}}.


waiting(info, {Perl, {data, Reply}}, Data = #data{perl=Perl, caller=Caller}) ->
    Decoded = decode(Reply),
    gen_statem:reply(Caller, {ok, Decoded}),
    {next_state, ready, Data#data{caller=undefined}};
waiting(info, {Perl, {exit_status, Status}}, #data{perl=Perl}) ->
    {stop, {perl_died, Status}}.


%%====================================================================
%% Private
%%====================================================================

find_perl_bin() ->
    case os:find_executable("perl") of
        false ->
            error("Cannot find Perl executable");
        Path ->
            Path
    end.

find_perl_code() ->
    case code:priv_dir(perlang5) of
        {error, Error} ->
            error(["Cannot find priv dir for perlang5: ", Error]);
        Path when is_list(Path) ->
            filename:join([Path, "perlang5.pm"])
    end.

encode(Term) ->
    erlang:term_to_binary(Term).

decode(Binary) ->
    erlang:binary_to_term(Binary).
